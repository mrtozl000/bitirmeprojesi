<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ForumController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserControlController;


 Route::get('aktif-degil', function (){
     return view('front.offline');
 });
 Route::get('/clear', function() {
     Artisan::call('cache:clear');
     Artisan::call('config:cache');
     Artisan::call('view:clear');
     return "Cleared!";
 });

Route::any('/',[HomeController::class, 'index']);

Route::any('/home',[HomeController::class, 'index'])->name('home.page');

Route::post('login',[LoginController::class, 'customLogin'])->name('login.custom');

Route::post('sign-up',[LoginController::class, 'customRegister'])->name('signup.custom');

Route::get('logout',[LoginController::class, 'customLogout'])->name('logout');

Route::get('profile',[ProfileController::class, 'profile'])->name('profile');

Route::post('profile',[ProfileController::class,'update'])->name('update.profile');

Route::get('forum',[ForumController::class, 'index']);

 Route::post('forum',[ForumController::class, 'newDiscussing'])->name('create-discussing');

 Route::get('/iletisim', [\App\Http\Controllers\ContactController::class, 'contact'])->name('contact');

 Route::post('/iletisim', [\App\Http\Controllers\ContactController::class, 'contactpost'])->name('contact.post');

 Route::get('/blogs', [\App\Http\Controllers\BlogController::class, 'index'])->name('homepage');

 Route::get('/category/{category?}', [\App\Http\Controllers\BlogController::class, 'category'])->name('category');

 Route::post('/category/{category?}', [\App\Http\Controllers\BlogController::class, 'category'])->name('category');

 Route::get('/blog/{category}/{slug?}', [\App\Http\Controllers\BlogController::class, 'single'])->name('single');

 Route::post('/blog/{category}/{slug?}', [\App\Http\Controllers\BlogController::class, 'single'])->name('single');

 Route::get("forum/discussing={id?}",[ForumController::class,'getDiscussingDetail']);

 Route::post('forum/discussing={id}',[ForumController::class, 'newReply']);

 Route::post('forum/discussing/delete={reply_delete}', [ForumController::class,'deleteReply'])->name('delete.reply');

Route::post('forum/discussing/update={reply_update}', [ForumController::class,'updateReply'])->name('update.reply');

Route::post('forum/discussing/update-discussing={id}', [ForumController::class,'updateDiscussing'])->name('update.discussing');

Route::post('forum/discussing/fixed-discussing={fixed_id}', [ForumController::class,'fixDiscussing'])->name('fixed.discussing');

Route::post('forum/discussing/delete-discussing={delete_id}',[ForumController::class, 'deleteDiscussing'])->name('delete.discussing');

Route::get('forum/discussing={id}/liked/reply={reply_id}', [ForumController::class , 'likedReply']);

Route::get('forum', [ForumController::class,'index'])->name('filtre');

Route::resource('admin-users-control',UserControlController::class);

Route::post('admin-users-control',[UserControlController::class,'search'])->name('search.admin');

Route::resource('yazilarim', ArticleController::class);

Route::get('hakkimizda', function (){
    return view('hakkimizda');
});

Route::get('deneme', function (){
    return view('deneme');
});
