@extends('Layouts.master')
@section('title', 'Tüm Makaleler')
@section('content')
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">@yield('title')</h6>
            <h6 class="m-0 font-weight-bold float-right text-primary">
                <strong>{{ $articles->count() }} makale bulundu.</strong>
            </h6>
            <a href="{{ route('yazilarim.create') }}" title="Düzenle" class="btn btn-sm btn-primary">Yeni Yazı Ekle</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Fotoğraf</th>
                        <th>Makale Başlığı</th>
                        <th>Kategori</th>
                        <th>Hit</th>
                        <th>Oluşturulma Tarihi</th>
                        <th>İşlemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($articles as $article)
                        <tr>
                            <td>
                                <img src="{{ asset($article->image) }}" width="200">
                            </td>
                            <td>{{ $article->title }}</td>
                            <td>{{ $article->getCategory->name }}</td>
                            <td>{{ $article->hit }}</td>
                            <td>{{ $article->created_at->diffForHumans() }}</td>
                            <td>
                                <a href="{{ route('single', [$article->getCategory->slug,$article->slug]) }}" target="_blank" title="Görüntüle" class="btn btn-sm btn-success">Görüntüle</a>
                                <a href="{{ route('yazilarim.edit',$article->id) }}" title="Düzenle" class="btn btn-sm btn-primary">Düzenle</a>
{{--                                <a href="{{ route('yazilarim.destroy', $article->id) }}" title="Sil" class="btn btn-sm btn-danger">Sil</a>--}}
                                <form method="POST" action="{{ route('yazilarim.destroy', $article->id) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <div class="form-group">
                                        <input type="submit" class="btn btn-danger delete" value="Sil">
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.delete').click(function(e){
            e.preventDefault() // Don't post the form, unless confirmed
            if (confirm('Silmek için emin misiniz?')) {
                // Post the form
                $(e.target).closest('form').submit() // Post the surrounding form
            }
        });
    </script>
@endsection
