<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Theme Made By www.w3schools.com -->
    <title>SorunBM-Hakkımızda</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        body {
            font: 400 15px Lato, sans-serif;
            line-height: 1.8;
            color: #818181;
        }

        h2 {
            font-size: 24px;
            text-transform: uppercase;
            color: #303030;
            font-weight: 600;
            margin-bottom: 30px;
        }

        h4 {
            font-size: 19px;
            line-height: 1.375em;
            color: #303030;
            font-weight: 400;
            margin-bottom: 30px;
        }

        .jumbotron {
            background-color: #f4511e;
            color: #fff;
            padding: 100px 25px;
            font-family: Montserrat, sans-serif;
        }

        .container-fluid {
            padding: 60px 50px;
        }

        .bg-grey {
            background-color: #f6f6f6;
        }

        .logo-small {
            color: darkslategray;
            font-size: 50px;
        }

        .logo {
            color: darkslategray;
            font-size: 200px;
        }

        .thumbnail {
            padding: 0 0 15px 0;
            border: none;
            border-radius: 0;
        }

        .thumbnail img {
            width: 100%;
            height: 100%;
            margin-bottom: 10px;
        }

        .carousel-control.right, .carousel-control.left {
            background-image: none;
            color: #f4511e;
        }

        .carousel-indicators li {
            border-color: #f4511e;
        }

        .carousel-indicators li.active {
            background-color: #f4511e;
        }

        .item h4 {
            font-size: 19px;
            line-height: 1.375em;
            font-weight: 400;
            font-style: italic;
            margin: 70px 0;
        }

        .item span {
            font-style: normal;
        }

        .panel {
            border: 1px solid #f4511e;
            border-radius: 0 !important;
            transition: box-shadow 0.5s;
        }

        .panel:hover {
            box-shadow: 5px 0px 40px rgba(0, 0, 0, .2);
        }

        .panel-footer .btn:hover {
            border: 1px solid #f4511e;
            background-color: #fff !important;
            color: #f4511e;
        }

        .panel-heading {
            color: #fff !important;
            background-color: #f4511e !important;
            padding: 25px;
            border-bottom: 1px solid transparent;
            border-top-left-radius: 0px;
            border-top-right-radius: 0px;
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
        }

        .panel-footer {
            background-color: white !important;
        }

        .panel-footer h3 {
            font-size: 32px;
        }

        .panel-footer h4 {
            color: #aaa;
            font-size: 14px;
        }

        .panel-footer .btn {
            margin: 15px 0;
            background-color: #f4511e;
            color: #fff;
        }

        .navbar {
            margin-bottom: 0;
            background-color: #f4511e;
            z-index: 9999;
            border: 0;
            font-size: 12px !important;
            line-height: 1.42857143 !important;
            letter-spacing: 4px;
            border-radius: 0;
            font-family: Montserrat, sans-serif;
        }

        .navbar li a, .navbar .navbar-brand {
            color: #fff !important;
        }

        .navbar-nav li a:hover, .navbar-nav li.active a {
            color: #f4511e !important;
            background-color: #fff !important;
        }

        .navbar-default .navbar-toggle {
            border-color: transparent;
            color: #fff !important;
        }

        footer .glyphicon {
            font-size: 30px;
            margin-bottom: 20px;
            color: darkslategray;
        }

        .slideanim {
            visibility: hidden;
        }

        .slide {
            animation-name: slide;
            -webkit-animation-name: slide;
            animation-duration: 1s;
            -webkit-animation-duration: 1s;
            visibility: visible;
        }

        @keyframes slide {
            0% {
                opacity: 0;
                transform: translateY(70%);
            }
            100% {
                opacity: 1;
                transform: translateY(0%);
            }
        }

        @-webkit-keyframes slide {
            0% {
                opacity: 0;
                -webkit-transform: translateY(70%);
            }
            100% {
                opacity: 1;
                -webkit-transform: translateY(0%);
            }
        }

        @media screen and (max-width: 768px) {
            .col-sm-4 {
                text-align: center;
                margin: 25px 0;
            }

            .btn-lg {
                width: 100%;
                margin-bottom: 35px;
            }
        }

        @media screen and (max-width: 480px) {
            .logo {
                font-size: 150px;
            }
        }

        #bizeSor img{
            border: 1px;
            border-radius: 10px;
            width: 400px;
            height: 240px;
        }
    </style>
</head>

<body id="sayfaBasi" data-spy="scroll" data-target=".navbar" data-offset="60">
<div id="geriDon" style="font-size: 30px">
    <a href="/" title="To Top">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
</div>

<div id="about" class="container-fluid">
    <div class="row">
        <div class="col-sm-8">
            <h2>BİZ KİMİZ?</h2><br>
            <h4>Arel Üniversitesinde okumakta olan Berat GÖZÜBÜYÜK ve Murat ÖZEL isimli iki insanız.</h4><br>
            <p> Berat IOT firmasında görev alan bir veri tabanı uzmanı olmakta olup Murat ise karaca.com'da PHP developer olarak görev almaktadır.</p>
            <p> Projemiz mesleki proje dersi için seçmiş olduğumuz, seçerken'de türkiyedeki yazılım alanındaki genel
                eksikliği gidermek amacıyla seçtiğimizi söyleyebiliriz ve çalışmalara başladığımızı dile getirmek de
                bizim için bir gurur kaynağı niteliğindedir diyebiliriz çünkü türkiyedeki yazılım alanında güçlü kolları,
            bilgisi, yetkinliği olan kişiler kendi blog sitelerinde paylaşımlar yaparak para kazanma derdinde olmakta olduğundan
            dolayı biz burdaki r10 net ve diğer benzer siteler gibi en uygun şekilde yardımcı olmak için çabalayan iki arkadaşız</p>
            <p> Sizlere her konuda destek olmaya çalışmak ta bizim görevlerimizden biri</p>
            <br>
            <div id="bizeSor">
                <img src="{{ URL::asset('images/bize-sor.jpeg') }}" alt="">
            </div>
            <br>
            <a href="/iletisim" class="btn btn-default btn-lg">Hadi Bizimle İletişime Geç</a>
        </div>
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-signal logo"></span>
        </div>
    </div>
</div>

<div class="container-fluid bg-grey">
    <div class="row">
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-globe logo slideanim"></span>
        </div>
        <div class="col-sm-8">
            <h2>Değerlerimiz</h2><br>
            <h4><strong>MİSYONUMUZ</strong><br> Bu projeyi yapma nedenimiz türkiyedeki yazılım sektöründe bulunan kişilerin tamamen
            türk kaynaklarından faydalana bilmesi için ve yetkinleklerimizi arttırmak için.</h4><br>
            <p><strong>VİZYONUMUZ </strong><br> Türkiyenin önde gelen soru cevap sitelerinden biri olmak, donanım haber gibi sitelerin farklı
            alanlarda yapacağımız projeler ile önüne geçebilmek ve gelecek için her zaman en yeni teknolojilere adapte olup bunları
            sizlerle buluşturmaktır.</p>
        </div>
    </div>
</div>

<footer class="container-fluid text-center">
    <a href="#sayfaBasi" title="To Top">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a>
</footer>

<script>
    $(document).ready(function () {
        $(".navbar a, footer a[href='#sayfaBasi']").on('click', function (event) {
            if (this.hash !== "") {
                event.preventDefault();

                var hash = this.hash;

                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 900, function () {

                    window.location.hash = hash;
                });
            }
        });

        $(window).scroll(function () {
            $(".slideanim").each(function () {
                var pos = $(this).offset().top;

                var winTop = $(window).scrollTop();
                if (pos < winTop + 600) {
                    $(this).addClass("slide");
                }
            });
        });
    })
</script>

</body>
</html>
