@extends('UserControl/master_user')
@section('userDetail')
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
    @endif
    <div class="container">
        <div class="float-right">
            <form method="POST" action="{{ route('admin-users-control.destroy', $user_info[0]->id) }}">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}

                <div class="form-group">
                    <input type="submit" class="btn btn-danger delete" value="Sil">
                </div>
            </form>
        </div>
        <div class="main-body">
            <div class="col-md-12">
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="d-flex align-items-end text-center" style="float: right">
                            <img src="{{ URL::asset('images/icons8-user-50.png') }}" alt="Admin"
                                 class="rounded-circle" width="150">
                            <div class="mt-3">
                                <h4>{{ $user_info[0]->nickname }}</h4>
                                <small>Üye Olduğu Tarih</small>
                                <h4>{{ $user_info[0]->created_at }}</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">İsim Soyisim</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                {{$user_info[0]->name}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Email</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                {{$user_info[0]->email}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Telefon</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                {{$user_info[0]->telephone}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-2">
                                <h6 class="mb-0">Kullanıcı Rolü</h6>
                            </div>
                            <div class="col-sm-3 text-secondary">
                                <form method="post" action="{{ route('admin-users-control.update',$user_info[0]->user_type_id) }}">
                                    @method('PUT')
                                    @csrf
                                    <input type="hidden" name="userId" id="userId" value="{{$user_info[0]->id}}">
                                    <select name="userType" id="userType">
                                        @foreach($usersType as $types)
                                            <option @if($types->user_type_id == $user_info[0]->user_type_id) selected @endif value="{{ $types->user_type_id }}">{{$types->user_type}}</option>
                                        @endforeach
                                    </select>
                                    <button class="btn" type="submit">güncelle</button>
                                </form>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="row gutters-sm">
                <div class="col-sm-6 mb-3">
                    <div class="card h-100">
                        <div class="card-body">
                            <h6 class="d-flex align-items-center mb-3"><i
                                    class="material-icons text-info mr-2"></i>Açtığı Konular</h6>
                            @if( count($tab) > 0)
                                @foreach($tab as $discussing)
                                    <small><a
                                            href="/forum/discussing={{ $discussing->discussing_id }}">{{ $discussing->discussing_title }}</a></small>
                                    <hr>
                                @endforeach
                        </div>
                        @else
                            <span class="mb-3 text-info"><a href="/forum"> Herhangi bir konunuz bulunmamaktadır!! konu oluşturmak Forum sayfamızı ziyaret edebilrisiniz..</a></span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6 mb-3">
                    <div class="card h-100">
                        <div class="card-body">
                            <h6 class="d-flex align-items-center mb-3"><i
                                    class="material-icons text-info mr-2"></i>Verdiği Yanıtlar</h6>
                            @if( count($tab2) > 0)
                                @foreach($tab2 as $replys)
                                    <small><a
                                            href="/forum/discussing={{ $replys['discussing_id'] }}">{{$replys->reply_text}}</a></small>
                                    <hr>
                                @endforeach
                        </div>
                        @else
                            <span class="mb-3 text-info"><a href="/forum"> Herhangi bir Yanıtınız bulunmamaktadır!! Yorum yapmak için tıklayınız.</a></span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.delete').click(function(e){
            e.preventDefault() // Don't post the form, unless confirmed
            if (confirm('Silmek için emin misiniz?')) {
                // Post the form
                $(e.target).closest('form').submit() // Post the surrounding form
            }
        });
    </script>
@endsection
