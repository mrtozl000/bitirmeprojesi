

@extends('Layouts.master')

@section('title', $category->name.' Kategorisi | '.count($articles).' yazı bulundu.')

@section('blog')

    <div class="col-md-9 mx-auto">
        @include('Widgets.articleList')
    </div>
    @include('Widgets.categoryWidget')
@endsection
