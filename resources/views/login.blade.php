<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="padding-top: 10px">
    <div class="modal-dialog" role="document">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="modal-content" style="margin-top: 100px;">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Giriş yap</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="false">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('login.custom') }}">
                @csrf
                <div class="modal-body mx-3">
                    <span class="text-danger">@error('email'){{$message}} @enderror</span>
                    <div class="md-form mb-2">
                        <i class="fas fa-envelope prefix grey-text"></i>
                        <input type="email" id="email" name="email" class="form-control validate">
                        <label for="email">Email</label>
                    </div>
                    <div class="md-form mb-2">
                        <i class="fas fa-lock prefix grey-text"></i>
                        <input type="password" id="password" name="password" class="form-control validate">
                        <label for="password">Şifre</label>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="submit" class="btn btn-default">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>

