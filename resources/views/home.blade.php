@extends('Layouts.master')
@section('title','Home')
@section('home')
    <div class="section-warp top-after-header"
         style="border-radius: 10px; background-image:url(https://2code.info/demo/themes/ask-me/wp-content/uploads/2014/06/desk_background.png);background-repeat:no-repeat;background-attachment:fixed;background-position-x:left;background-position-y:top;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">
        <div class="container clearfix">
            <div class="box_icon box_warp box_no_border box_no_background">
                <div class="row">
                    <div class="col-md-3"><h2 style="color: whitesmoke">SorunBM'e Hoşgeldiniz</h2>
                        <p style="color: whitesmoke">Bu projenin amacı bilgiyi paylaşmak ve
                            paylaştıkça büyümektedir, en önemlisi bilginizi paylaşmayı unutmayın!!</p>
                        <div class="clearfix"></div>
                        <a class="dropdown-item" style="color: whitesmoke" href="/hakkımızda">Hakkımızda</a>
                        <a data-toggle="modal" style="color: whitesmoke" data-target="#modalLoginForm"
                           class="dropdown-item"
                        ><span
                                class="mr-2 icon-lock_outline"></span>içeri Gel</a></div>
                    <div class="col-md-9" style="display: inline-block; position: relative;">
                        <form class="form-style form-style-2" method="post"
                              action="https://2code.info/demo/themes/ask-me/ask-question/">
                            <p><textarea style="border-radius: 10px; display: block; font-size: 16px" name="title" rows="10"
                                         id="question_title"
                                         onfocus="if(this.value==this.defaultValue)this.value='';"
                                         onblur="if(this.value=='')this.value=this.defaultValue;">Hadi Bize Sor !!!</textarea>
                            </p>
                            <button class="btn btn-info ask-question float-right" style="position: absolute; bottom: 16px; right: 15px">Ask Now
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm">
                <h3 style="text-align: center; color: whitesmoke"> Son Oluşturulan 3 Konu </h3>
                @foreach($tab as $items)
                    <div class="p-2 p-sm-3 forum-content">
                        <div class="card mb-2">
                            <div class="card-body p-2 p-sm-3">
                                <div class="media forum-item"
                                     data-id="{{ $items['discussing_id'] }}">
                                    <img
                                        src="{{ URL::asset('images/icons8-user-50.png') }}"
                                        class="mr-3 rounded-circle" width="50" alt="User"/>
                                    <div class="media-body">
                                        <a href="forum/discussing={{ $items['discussing_id'] }}">
                                            <span><b>{{ $items['discussing_title'] }}</b></span></a>
                                        <h6><p class="text-body">{{ $items['name']}}</p></h6>
                                        <p class="text-secondary">
                                            {{ $items['discussing_text'] }}
                                        </p>
                                        <p class="text-muted">Tarihinde</a> Oluşturuldu <span
                                                class="text-secondary font-weight-bold">{{ $items['date_added'] }}</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach
            </div>
            <div class="col-md-6 col-sm">
                <h3 style="text-align: center; color: whitesmoke"> En Çok Beğenilen 3 Yorum </h3>
                @foreach( $tab2 as $replys)
                    <div class="p-2 p-sm-3 forum-content">
                        <div class="card mb-2">
                            <div class="card-body p-2 p-sm-3">
                                <div class="media forum-item"
                                     data-id="{{ $items['discussing_id'] }}">
                                    <img
                                        src="{{ URL::asset('images/icons8-user-50.png') }}"
                                        class="mr-3 rounded-circle" width="50" alt="User"/>
                                    <div class="media-body">
                                        <a href="forum/discussing={{ $replys['discussing_id'] }}">
                                            <span><b>Konuyu Görüntüle</b></span></a>
                                        <h6><p class="text-body">{{ $replys['name']}}</p></h6>
                                        <p class="text-secondary">
                                            {{ $replys['reply_text'] }}
                                        </p>
                                        <p class="text-muted">Tarihinde</a> Oluşturuldu <span
                                                class="text-secondary font-weight-bold">{{ $items['date_added'] }}</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="container">
        <h3 style="text-align: center; color: whitesmoke"> Bu Yazılar Dikkatinizi Çekebilir. </h3>
        <br>
        <div class="d-flex justify-content-center">
            @foreach ($tab3 as $article)
                <div class="post-preview" style="margin-left: 15px;">
                    <a href="{{ route('single', [$article->getCategory->slug, $article->slug]) }}"
                       style="color: mintcream">
                        <h2 class="text-edit">
                            {{ $article->title }}
                        </h2>
                        <img src="{{ $article->image }}" style="border: 2px ; border-radius: 10px; max-height: 50%; max-width: 50%;" alt="">
                        <p class="post-subtitle">
                            {!! Str::limit($article->content,75) !!}
                        </p>
                    </a>
                    <p class="post-meta"> Kategori :
                        <a href="#">{{ $article->getCategory->name }}</a>
                        <span class="float-right">{{ $article->created_at->diffForHumans() }}</span></p>
                </div>
            @endforeach
        </div>
    </div>
@endsection

