@component('mail::message')
    {{ $mailInfo['title'] }}

    Bizimle iletişime geçtiğiniz için teşekkür ederiz...

    {{ $mailInfo['body'] }}

    @component('mail::button', ['url' => $mailInfo['url'],])
        Ana sayfaya geri dön!!!
    @endcomponent

    Teşekkürler,<br>
    {{ config('app.name') }}
@endcomponent
