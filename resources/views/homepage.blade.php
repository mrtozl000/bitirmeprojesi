

@extends('Layouts.master')

@section('title','Bloglar')

@section('blog')

    <div class="col-md-9 mx-auto">
        <br>
        @include('Widgets.articleList')
    </div>
    @include('Widgets.categoryWidget')
@endsection
