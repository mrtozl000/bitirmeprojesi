
@extends('Layouts.master')
@section('title',$article->title)
@section('bg',$article->image)
@section('blog')
    <div class="col-md-9 mx-auto">
        <br>
        <img src="{{ $article->image }}" alt="" style="border: 2px; border-radius: 10px">
        <div class="mt-5" style="color:mintcream">{!! $article->content !!}</div>
        <br><br>
        <span class="text-info">Okunma sayısı : <b>{{ $article->hit }}</b></span>
    </div>
    @include('Widgets.categoryWidget')
@endsection
