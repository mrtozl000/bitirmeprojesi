@extends('Layouts.master')
@section('title','Profile')
@section('Profile')
    @auth()
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="container">
            <div class="main-body">
                <div class="row gutters-sm">
                    <div class="col-md-4 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-column align-items-center text-center">
                                    <img src="{{ URL::asset('images/icons8-user-50.png') }}" alt="Admin"
                                         class="rounded-circle" width="150">
                                    <div class="mt-3">
                                        <h4>{{ $user_info[0]->nickname }}</h4>
                                        <small>Üye Olduğunuz Tarih</small>
                                        <h4>{{ $user_info[0]->created_at }}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card mb-3">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <h6 class="mb-0">İsim Soyisim</h6>
                                    </div>
                                    <div class="col-sm-9 text-secondary">
                                        {{$user_info[0]->name}}
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <h6 class="mb-0">Email</h6>
                                    </div>
                                    <div class="col-sm-9 text-secondary">
                                        {{$user_info[0]->email}}
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <h6 class="mb-0">Telefon</h6>
                                    </div>
                                    <div class="col-sm-9 text-secondary">
                                        {{$user_info[0]->telephone}}
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <h6 class="mb-0">Kullanıcı Rolü</h6>
                                    </div>
                                    <div class="col-sm-9 text-secondary">
                                        {{$user_info[0]->user_type}}
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <a class="btn btn-info "
                                           data-toggle="modal" data-target="#editUserInfo">Edit</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row gutters-sm">
                            <div class="col-sm-6 mb-3">
                                <div class="card h-100">
                                    <div class="card-body">
                                        <h6 class="d-flex align-items-center mb-3"><i
                                                class="material-icons text-info mr-2">Atanmış</i>Konularınız</h6>
                                        @if( count($tab) > 0)
                                            @foreach($tab as $discussing)
                                                <small><a
                                                        href="forum/discussing={{ $discussing->discussing_id }}">{{ $discussing->discussing_title }}</a></small>
                                                <hr>
                                            @endforeach
                                            {{ $tab->links() }}
                                    </div>
                                    @else
                                        <span class="mb-3 text-info"><a href="/forum"> Herhangi bir konunuz bulunmamaktadır!! konu oluşturmak Forum sayfamızı ziyaret edebilrisiniz..</a></span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6 mb-3">
                                <div class="card h-100">
                                    <div class="card-body">
                                        <h6 class="d-flex align-items-center mb-3"><i
                                                class="material-icons text-info mr-2">Atanmış</i>Yanıtlarınız</h6>
                                        @if( count($tab2) > 0)
                                            @foreach($tab2 as $replys)
                                                <small><a
                                                        href="forum/discussing={{ $replys['discussing_id'] }}">{{$replys->reply_text}}</a></small>
                                                <hr>
                                            @endforeach
                                            {{ $tab2->links() }}
                                    </div>
                                    @else
                                        <span class="mb-3 text-info"><a href="/forum"> Herhangi bir Yanıtınız bulunmamaktadır!! Yorum yapmak için tıklayınız.</a></span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endauth

@endsection


<div class="modal fade" id="editUserInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="padding-top: 50px">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">üye ol</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="false">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('update.profile') }}">
                @csrf
                <div class="modal-body mx-3">
                    <div class="row gx-3 mb-3">
                        <div class="form-group col-md-6">
                            <label for="name">İsim</label>
                            <input type="text" class="form-control" name="name" id="name"
                                   value="{{ $user_info[0]->name }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="surname">Nickname</label>
                            <input type="text" class="form-control" name="nickname" id="nickName"
                                   value="{{ $user_info[0]->nickname }}">
                        </div>
                    </div>
                    <div class="row gx-3 mb-3">
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="text" id="email" name="email" class="form-control"
                                   value="{{ $user_info[0]->email }}"
                                   readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="telephone">Telefon</label>
                            <input type="text" class="form-control" name="telephone" id="telephone"
                                   value="{{ $user_info[0]->telephone }}">
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <input type="hidden" class="form-control" name="id" id="telephone"
                               value="{{ \Illuminate\Support\Facades\Auth::id() }}">
                    </div>
                    <button type="submit" class="btn btn-primary">Hesap bilgilerini güncelle</button>
                </div>
            </form>
        </div>
    </div>
</div>
