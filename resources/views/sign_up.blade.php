<div class="modal fade" id="modalSignUpForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="padding-top: 50px">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="margin-top: 100px;">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">üye ol</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="false">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('signup.custom') }}">
                @csrf
                <div class="modal-body mx-3">
                    <div class="md-form mb-2">
                        <input type="text" id="name" name="name" class="form-control" required>
                        <label for="name">İsim Soyisim</label>
                        <span class="text-danger">@error('name'){{$message}} @enderror</span>
                    </div>
                    <div class="md-form mb-2">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" id="nickname" name="nickname" class="form-control" required>
                        <label for="nickname">Kullanıcı adı</label>
                        <span class="text-danger">@error('nickname'){{$message}} @enderror</span>
                    </div>
                    <div class="md-form mb-2">
                        <i class="fas fa-envelope prefix grey-text"></i>
                        <input type="email" id="email" name="email" class="form-control" required>
                        <label for="email">Email</label>
                        <span class="text-danger">@error('email'){{$message}} @enderror</span>
                    </div>
                    <div class="md-form mb-2">
                        <i class="fas fa-phone prefix grey-text"></i>
                        <input type="tel" id="telephone" name="telephone" placeholder="10 Haneli Cep numaranızı giriniz" class="form-control">
                        <label for="telephone">telephone</label>
                    </div>
                    <div class="md-form mb-2">
                        <i class="fas fa-lock prefix grey-text"></i>
                        <input type="password" id="password" name="password" class="form-control" required>
                        <label for="password">Şifre</label>
                        <span class="text-danger">@error('password'){{$message}} @enderror</span>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="submit" class="btn btn-default">Üye Ol</button>
                </div>
            </form>
        </div>
    </div>
</div>

