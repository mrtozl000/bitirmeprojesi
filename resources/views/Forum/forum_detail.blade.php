<head>
    <link rel="stylesheet" href="/css/forum.css">
</head>
@extends('Layouts.master')
@section('title','forum')
@section('forum_detail')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css"
          integrity="sha256-46r060N2LrChLLb5zowXQ72/iKKNiw/lAmygmHExk/o=" crossorigin="anonymous"/>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if (session('Success'))
        <div class="alert alert-success">
            {{ session('Success') }}
        </div>
    @endif
    <div class="inner-main-body p-2 p-sm-3 forum-content container">
        <a href="/forum" class="btn btn-light btn-sm mb-3 has-icon"
        ><i class="fa fa-arrow-left mr-2"></i>Geri</a>
        @if(($discussing->status) == 1)
            <div class="alert alert-secondary">Konu çözülmüştür.</div>
        @endif
        <div class="card mb-5">
            <div class="card-body">
                <div class="media forum-item">
                    <p class="card-link">
                        <img src="{{ URL::asset('images/icons8-user-50.png') }}"
                             class="rounded-circle" width="50" alt="User"/>
                    </p>
                    <div class="media-body ml-3">
                        <p class="text-secondary">{{ $discussing->name }}</p>
                        <small
                            class="text-muted ml-2">{{   $discussing->date_added }} Tarihinde oluşturuldu</small>
                        <h5 class="mt-1">{{ $discussing->discussing_title }}</h5>
                        <div class="mt-3 font-size-sm">
                            <p>
                                {{ $discussing->discussing_text }}
                            </p>
                            @isset($items['image'])
                                <img src="{{$items['image']}}" style="border: 2px ; border-radius: 10px">
                            @endisset
                        </div>
                    </div>
                    <div class="text-muted text-center">
                        <span class="d-none d-sm-inline-block"><i class="far fa-eye"></i> {{ $discussing->click_count }}</span>
                        <span><i class="far fa-comment ml-2"></i> {{ $countFromReply  }}</span>
                    </div>
                    <div class="d-flex align-items-end m-md-1">
                        @if(($discussing->status) != 1)
                            @auth()
                            @if( $discussing->created_user_id == Auth::id() || in_array(auth()->user()->user_type_id,[1,2]) )
                                <tbody>
                                <tr>
                                    <td>
                                        <button class="btn btn-outline-info ur" style="margin-left: 10px"
                                                data-toggle="modal"
                                                data-target="#discussingUpdate">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                 fill="currentColor" class="bi bi-arrow-clockwise" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd"
                                                      d="M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2v1z"/>
                                                <path
                                                    d="M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466z"/>
                                            </svg>
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <form id="fixed">
                                            <input type="hidden" name="fixedDiscussing" value="1">
                                            <button class="btn btn-outline-success fix"
                                                    style="position: absolute; margin-top: 5px"
                                                    type="submit">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                     fill="currentColor" class="bi bi-check2-all" viewBox="0 0 16 16">
                                                    <path
                                                        d="M12.354 4.354a.5.5 0 0 0-.708-.708L5 10.293 1.854 7.146a.5.5 0 1 0-.708.708l3.5 3.5a.5.5 0 0 0 .708 0l7-7zm-4.208 7-.896-.897.707-.707.543.543 6.646-6.647a.5.5 0 0 1 .708.708l-7 7a.5.5 0 0 1-.708 0z"/>
                                                    <path
                                                        d="m5.354 7.146.896.897-.707.707-.897-.896a.5.5 0 1 1 .708-.708z"/>
                                                </svg>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <form method="post"
                                              action="{{ route('delete.discussing',$discussing->discussing_id) }}">
                                            @csrf
                                            <input type="hidden" name="discussingId" id="discussingId"
                                                   value="{{ $discussing->discussing_id }}"/>
                                            <button type="submit" class="btn btn-outline-danger"
                                                    onclick="return confirm('Silmek istediğinizden emin misiniz?')">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                     fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                                    <path
                                                        d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                                    <path fill-rule="evenodd"
                                                          d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                                </svg>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                </tbody>
                            @endif
                            @endauth
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @if($countFromReply > 0)
            @foreach( $reply as $replys)
                <div class="overflow-auto">
                    <div class="card mb-2">
                        <div class="card-body">
                            <div class="media forum-item">
                                <a href="javascript:void(0)" class="card-link">
                                    <img src="{{ URL::asset('images/icons8-user-50.png') }}"
                                         class="rounded-circle" width="50" alt="User"/>
                                </a>
                                <div class="media-body ml-3">
                                    <a href="javascript:void(0)" class="text-secondary">{{ $replys->name }}</a>
                                    <small class="text-muted ml-2">{{ $replys->date_added }}</small>
                                    <div class="mt-3 font-size-sm">
                                        <p>
                                            {{ $replys->reply_text }}
                                        </p>
                                    </div>
                                </div>
                                @if($replys->user_id == Auth::id() || in_array($replys->user_type_id,[1,2])  )
                                    <div class="text-muted small text-center">
                                        <form class="d-none d-sm-inline-block" method="post"
                                              action="{{ route('delete.reply',$discussing->discussing_id) }}">
                                            @csrf
                                            <input type="hidden" name="deleteReplyById" id="deleteReplyById"
                                                   value="{{ $replys->reply_id }}"/>
                                            <button type="submit" class="btn btn-outline-danger"
                                                    onclick="return confirm('Silmek istediğinizden emin misiniz?')">Sil
                                            </button>
                                        </form>
                                    </div>
                                    <!-- onclick="return confirm('Güncellemek istediğinizden emin misiniz?')"-->
                                    <button class="btn btn-outline-info ur" style="margin-left: 10px"
                                            data-toggle="modal"
                                            data-target="#replyUpdate" data-id="{{$replys->reply_id }}">
                                        Güncelle
                                    </button>
                                @endif
                                <div class="ml-1 align-items-center" style="
                                         display: flex;
                                         font-size: 12px;
                                         justify-content: center;">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                         class="bi bi-heart" viewBox="0 0 16 16"
                                         style="width: 50px; height: 38px; position: relative">
                                        <path
                                            d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                    </svg>
                                    <a href="/forum/discussing={{ $discussing->discussing_id }}/liked/reply={{$replys->reply_id}}"
                                       style="color: red; position: absolute; opacity: 0.8">{{ $replys->reply_liked_count }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            @auth
                <button class="btn btn-primary has-icon align-items-lg-center float-right" style="font-size: 12px"
                        type="button"
                        data-toggle="modal"
                        data-target="#threadModal">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                         stroke-linejoin="round" class="feather feather-plus mr-2">
                        <line x1="12" y1="5" x2="12" y2="19"></line>
                        <line x1="5" y1="12" x2="19" y2="12"></line>
                    </svg>
                    Cevapla
                </button>
            @endauth
        @else
            @auth
                <p class="noselect" data-toggle="modal"
                   data-target="#threadModal" id="reply" style="text-align: center">Henüz cevap
                    bulunmamaktadır lütfen
                    cevaplmak için tıklayınız !!!</p>
            @endauth
            @guest
                <p class="noselect"
                   id="reply" style="text-align: center">Henüz cevap
                    bulunmamaktadır lütfen
                    cevaplmak <a href="" data-toggle="modal" data-target="#modalSignUpForm"> üye olunuz</a>
                    ve ya <a href="" data-toggle="modal" data-target="#modalLoginForm"> giriş yapınız</a> !!!</p>
            @endguest
        @endif
        <span class="float-left btn mt-auto"> {{ $reply->links() }} </span>
    </div>

    <div class="modal fade" id="threadModal" tabindex="-1" role="dialog" aria-labelledby="threadModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" style="margin-top: 15%" role="document">
            <div class="modal-content">
                <form method="post" action="{{ url('forum/discussing='.$discussing->discussing_id) }}">
                    @csrf
                    <div class="modal-header d-flex align-self-center bg-primary text-white">
                        <h6 class="modal-title mb-0" id="threadModalLabel">Hadi Cevaplayalım</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div>
                                <label for="summernote">Notunuzu Giriniz</label>
                                <textarea class="form-control summernote" id="summernote"
                                          name="summernote"></textarea>
                            </div>
                            <div class="custom-file form-control-sm mt-3" style="max-width: 300px;">
                                <input type="file" class="custom-file-input" id="customFile" name="customFile"
                                       multiple=""/>
                                <label class="custom-file-label" for="customFile">Attachment</label>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-light" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Post</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- yorum güncelleme modalı -->
    <div class="modal fade" id="replyUpdate" tabindex="-1" role="dialog"
         aria-labelledby="threadModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 15%" role="document">
            <div class="modal-content">
                <div class="modal-header">Notunuzu Giriniz</div>
                <div class="modal-body">
                    <form class="d-none d-sm-inline-block" method="post"
                          action="{{ route('update.reply' , $discussing->discussing_id) }}">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="updateReplyById"
                                   id="updateReplyById"/>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" cols="50" id="updateNote" name="updateNote"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-info">
                                Güncelle
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- konu güncelleme modalı -->
    <div class="modal fade" id="discussingUpdate" tabindex="-1" role="dialog"
         aria-labelledby="discussingModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 15%" role="document">
            <div class="modal-content">
                <div class="modal-header">Güncellemek istediğinizi Alanları düzenleyiniz.</div>
                <div class="modal-body">
                    <form class="d-none d-sm-inline-block" method="post"
                          action="{{ route('update.discussing' , $discussing->discussing_id) }}">
                        @csrf
                        <div class="form-group">
                            <label for="updateDiscussungTitle">Başlık</label>
                            <input type="text" class="form-control" name="updateDiscussingTitle"
                                   id="updateDiscussingTitle" value="{{ $discussing->discussing_title }}"/>
                        </div>
                        <div class="form-group">
                            <label for="updateDiscussingText">Açıklama</label>
                            <textarea class="form-control" cols="50" id="updateDiscussingText"
                                      name="updateDiscussingText">{{ $discussing->discussing_text }}</textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-info">
                                Güncelle
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).on('click', '.ur', function () {
            var data = $(this).attr('data-id');
            $("#updateReplyById").val(data);
        })

        $('#fixed').on('submit', function (e) {
            e.preventDefault();

            let fixed = $('input[name=fixedDiscussing]').val();
            let _token = $('meta[name=csrf-token]').attr('content');
            $.ajax({
                type: 'POST',
                url: "{{route('fixed.discussing',$discussing->discussing_id)}}",
                data: {
                    _token: _token,
                    fixed: fixed,
                },
                datatype: 'json',
                success: function (response) {
                    location.reload();
                },
            });
        });


    </script>
@endsection

