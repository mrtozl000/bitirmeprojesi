<head>
    <link rel="stylesheet" href="/css/forum.css">
</head>
@extends('Layouts.master')
@section('title','forum')
@section('forum_list')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if (session('Success'))
        <div class="alert alert-success">
            {{ session('Success') }}
        </div>
    @endif
    <div class="container-fluid">
        <div class="main-body p-0 mt-5">
            <div class="inner-wrapper">
                <div class="inner-sidebar">
                    <div class="inner-sidebar-body p-0">
                        <div class="p-3 h-100" data-simplebar="init">
                            <div class="simplebar-wrapper" style="margin: -16px;">
                                <div class="simplebar-mask">
                                    <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                                        <div class="simplebar-content-wrapper"
                                             style="height: 100%;">
                                            <div class="simplebar-content" style="padding: 16px;">
                                                <nav class="nav nav-pills nav-gap-y-1 flex-column" id="leftFilter">
                                                    <a href="/forum"
                                                       class="nav-link nav-link-faded has-icon tab">Tüm konular</a>
                                                    <a href="?populer"
                                                       class="nav-link nav-link-faded has-icon tab">Tüm Zamanların
                                                        Popüleri</a>
                                                    <a href="?resolved"
                                                       class="nav-link nav-link-faded has-icon tab">Çözüldü</a>
                                                    <a href="?not-yet-resolved"
                                                       class="nav-link nav-link-faded has-icon tab">Çözülmedi</a>
                                                    <a href="?no-answer-given"
                                                       class="nav-link nav-link-faded has-icon tab">Henüz
                                                        cevaplanmadı</a>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="inner-main">
                    <form id="filtre" method="get" action="{{route('filtre')}}">
                        <div class="inner-main-header form-group">
                            <select class="custom-select custom-select-sm w-auto mr-1 form-control"
                                    name="category">
                                <option value="0">Kategoriler</option>
                                @foreach($category as $categories)
                                    <option
                                        value="{{ $categories->category_id }}">{{$categories->category_name}}</option>
                                @endforeach
                            </select>
                            <span class="input-icon input-icon-sm ml-auto w-auto" style="margin-right:5px ">
                             <input type="text"
                                    class="form-control form-control-sm bg-gray-200 border-gray-200 shadow-none mb-4 mt-4"
                                    placeholder="Search forum" name="q"/>
                            </span>
                            <button type="submit" class="btn btn-primary search">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                    <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                                </svg>
                            </button>
                            <div class="ml-5">
                                @auth
                                    <button class="btn btn-primary has-icon" style="font-size: 12px" type="button"
                                            data-toggle="modal"
                                            data-target="#threadModal">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24"
                                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                             stroke-linejoin="round" class="feather feather-plus mr-2">
                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                        </svg>
                                        Yeni Konu
                                    </button>
                                @endauth
                                @guest
                                    <button class="btn btn-info" type="button" data-toggle="modal"
                                            data-target="#modalSignUpForm" style="font-size: 12px">
                                        Konu açmak için lütfen üye olunuz!
                                    </button>
                                @endguest
                            </div>
                        </div>
                    </form>
                    <div class="overflow-auto">
                        @foreach($list as $items)
                            <div class="p-2 p-sm-3 collapse forum-content show">
                                <div class="card mb-2">
                                    <div class="card-body p-2 p-sm-3">
                                        <div class="media forum-item"
                                             data-id="{{ $items['discussing_id'] }}">
                                            <img
                                                src="{{ URL::asset('images/icons8-user-50.png') }}"
                                                class="mr-3 rounded-circle" width="50" alt="User"/>
                                            <div class="media-body">
                                                <a href="forum/discussing={{ $items['discussing_id'] }}">
                                                    <span><b>{{ $items['discussing_title'] }}</b></span></a>
                                                <h6><p class="text-body">{{ $items['name']}}</p></h6>
                                                <p class="text-secondary">
                                                    {{ $items['discussing_text'] }}
                                                </p>
                                                @isset($items['image'])
                                                    <img src="{{$items['image']}}"
                                                         style="border: 2px ; border-radius: 10px">
                                                @endisset
                                                <p class="text-muted">Tarihinde</a> Oluşturuldu <span
                                                        class="text-secondary font-weight-bold">{{ $items['date_added'] }}</span>
                                                </p>
                                            </div>
                                            <div class="text-muted small text-center align-self-center">
                                                        <span class="d-none d-sm-inline-block"><i
                                                                class="far fa-eye"></i> {{ $items['click_count'] }}</span>
                                                <span><i
                                                        class="far fa-comment ml-2"></i> {{ $items['reply_count'] }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <nav aria-label="Page navigation" style="display: flex; justify-content: center">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" href="/forum">İlk</a></li>
                    <div style="display: none">{{ $x = pathinfo(Request::fullUrl())['basename'] }} </div>
                    @for($p = 1 ; $p <= $page ; $p++ )
                        @if(strpos($x,'?',4)==true)
                            <li class="page-item"><a class="page-link x"
                                                     href="{{Request::fullUrl()}}&page={{$p}}">{{$p}}</a></li>
                        @else
                            <li class="page-item"><a class="page-link x"
                                                     href="?page={{$p}}">{{$p}}</a></li>
                        @endif
                    @endfor
                    <li class="page-item"><a class="page-link x" href="?page={{$p-1}}">son</a></li>
                </ul>
            </nav>
        </div>
    </div>
@endsection
<div class="modal fade" id="threadModal" tabindex="-1" role="dialog" aria-labelledby="threadModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" style="margin-top: 15%" role="document">
        <div class="modal-content">
            <form method="post" action="{{ route('create-discussing') }}">
                <div class="modal-header d-flex align-self-center bg-primary text-white">
                    <h6 class="modal-title mb-0" id="threadModalLabel">Yeni Konu</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        @csrf
                        <div>
                            <label for="threadTitle">Başlık</label>
                            <input type="text" class="form-control" id="threadTitle" name="threadTitle"
                                   placeholder="Başlığı Giriniz." autofocus=""/>
                        </div>
                        <div>
                            <label for="category">Kategori</label>
                            <select class="forum-control" name="category" id="category">
                                <option value="0">Seçiniz</option>
                                @foreach($category as $categories)
                                    <option
                                        value="{{ $categories->category_id }}">{{$categories->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div>
                            <label for="summernote">Notunuzu Giriniz</label>
                            <textarea class="form-control summernote" id="summernote"
                                      name="summernote"></textarea>
                        </div>
                        <div class="custom-file form-control-sm mt-3" style="max-width: 300px;">
                            <input type="file" class="custom-file-input" id="customFile" name="customFile"
                                   multiple=""/>
                            <label class="custom-file-label" for="customFile">Attachment</label>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-light" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Post</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

<script>
    $(document).ready(function () {
        $("#leftFilter a").click(function () {
            $("#leftFilter a").removeClass("active");
            $(this).addClass("active");
        });
    });

</script>

