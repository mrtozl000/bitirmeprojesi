<!doctype html>
<html lang="en" style="background-color: darkslategray;">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/jquery.sticky.js"></script>
    @yield('js')

    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/fonts/icomoon/icom.css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/style.css">
    @yield('css')
</head>
<header class="web-header">
    <div class="site-navbar container-fluid">
        <div class="row align-items-center" id="header">
            <div class="site-logo col-6"><a href="/">SorunBM</a></div>
            <nav class="mx-auto site-navigation header-for-none">
                <ul class="site-menu js-clone-nav d-none d-xl-block ml-0 pl-0">
                    <li><a href="/" class="nav-link">Ana Sayfa</a></li>
                    <li><a href="/hakkimizda">Hakkımızda</a></li>
                    <li class="has-children">
                        <a href="#">Sayfalar</a>
                        <ul class="dropdown">
                            <li><a href="/forum">Forum</a></li>
                            <li><a href="{{ route('homepage') }}">Blog</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ route('contact') }}">İletişim</a></li>
                </ul>
            </nav>
            <div class="right-cta-menu d-flex text-center  col-6">
                <div class="ml-auto navbar navbar-dark navbar-expand-sm">
                    <div class="navbar-collapse">
                        <ul class="navbar-nav col-6">
                            <li class="nav-item dropdown">

                                <a class="nav-link dropdown-toggle"
                                   href=".dropdown-menu"
                                   id="navbarDropdownMenuLink"
                                   role="button" data-toggle="dropdown"
                                   aria-haspopup="true"
                                   aria-expanded="false">
                                    <img
                                        src="{{ URL::asset('images/icons8-user-50.png') }}"
                                        width="40" height="40" class="rounded-circle">
                                </a>
                            </li>
                        </ul>
                        @auth
                            <div class="dropdown-menu"
                                 aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('profile') }}">Profili
                                    Görüntüle</a>
                                <a class="dropdown-item" href="{{ route('logout') }}">Çıkış Yap</a>
                                @if((auth()->user()->user_type_id)==1)
                                    <a class="dropdown-item" href="/{{'admin-users-control'}}">kullanıcı paneli</a>
                                @endif
                                @if(!empty(Auth::user()->id ?? 0) && in_array(Auth::user()->user_type_id ?? 0, [1, 3]))
                                    <a href="{{ route('yazilarim.index') }}" class="dropdown-item">Blog Yazılarım</a>
                                @endif
                            </div>
                        @endauth
                        @guest
                            <div class="dropdown-menu"
                                 aria-labelledby="navbarDropdownMenuLink">
                                <a href="" data-toggle="modal" data-target="#modalSignUpForm"
                                   class="dropdown-item"><span
                                        class="mr-2 icon-sign-in"></span>Üye ol</a>
                                <a href="" data-toggle="modal" data-target="#modalLoginForm"
                                   class="dropdown-item"
                                ><span
                                        class="mr-2 icon-lock_outline"></span>Giriş yap</a>
                            </div>
                        @endguest
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<body>
<div class="mb-5">
    @include('login')
    @include('sign_up')
    @yield('Profile')
    @yield('home')
    <div class="container">
        @yield('forum_detail')
    </div>
    @yield('forum_list')
    @yield('blog')
    @yield('content')
</div>
</body>
<footer id="footer" class="font-small cyan darken-3 position-relative">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="widget widget_contact">
                    <h3 class="widget_title">Neredeyiz ?</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi adipiscing gravida odio,
                        sit amet suscipit risus ultrices eu.</p>
                    <ul>
                        <li>
                            <span>Address :</span>
                            İstanbul Arel Üniversitesi
                        </li>
                        <li>
                            <span>Destek :</span>Support Telephone No : (+2)01111011110
                        </li>
                        <li>Destek Email : arel@arel.com</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="widget">
                    <h3 class="widget_title">Hızlı Gez</h3>
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li><a href="/forum">Forum</a></li>
                        <li><a href="/hakkimizda">Hakkımızda</a></li>
                        <li><a href="/blogs">Blog</a></li>
                        <li><a href="/iletisim">Bizimle iletişime Geç</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget">
                    <h3 class="widget_title">Social Media</h3>
                    <ul>
                        <li class="twitter"><a original-title="Twitter" class="tooltip-n" href="#"><i
                                    class="social_icon-twitter font17"></i>Twitter</a></li>
                        <li class="facebook"><a original-title="Facebook" class="tooltip-n" href="#"><i
                                    class="social_icon-facebook font17"></i>Facebook</a></li>
                        <li class="gplus"><a original-title="Google plus" class="tooltip-n" href="#"><i
                                    class="social_icon-gplus font17"></i>Google</a></li>
                        <li class="youtube"><a original-title="Youtube" class="tooltip-n" href="#"><i
                                    class="social_icon-youtube font17"></i>Youtube</a></li>
                        <li class="skype"><a original-title="Skype" class="tooltip-n" href="skype:#?call"><i
                                    class="social_icon-skype font17"></i>Skype</a></li>
                        <li class="flickr"><a original-title="Flickr" class="tooltip-n" href="#"><i
                                    class="social_icon-flickr font17"></i>Flickr</a></li>
                        <li class="rss"><a original-title="Rss" class="tooltip-n" href="#"><i
                                    class="social_icon-rss font17"></i>Rss</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="copyrights f_left" style="color: cornflowerblue; padding-top: 20px">Copyright 2022 Arel Üniversitesi
            | <a href="#">By 2code</a></div>
    </div>
</footer>
</html>

