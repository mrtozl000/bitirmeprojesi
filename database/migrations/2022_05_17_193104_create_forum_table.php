<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum', function (Blueprint $table) {
            $table->id('discussing_id');
            $table->foreignId('created_user_id')->references('id')->on('users');
            $table->string('discussing_title');
            $table->text('discussing_text');
            $table->foreignId('discussing_category_id')->references('category_id')->on('category');
            $table->integer('click_count');
            $table->dateTime('date_added');
            $table->tinyInteger('status')->default(1);
            $table->text('image');
            $table->timestamps();

            $table->index(['discussing_id','created_user_id','date_added']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forum');
    }
};
