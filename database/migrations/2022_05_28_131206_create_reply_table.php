<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reply', function (Blueprint $table) {
            $table->id('reply_id');
            $table->foreignId('discussing_id')->references('discussing_id')->on('forum');
            $table->foreignId('user_id')->references('id')->on('users');
            $table->text('reply_text');
            $table->dateTime('date_added');
            $table->integer('reply_liked_count')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reply');
    }
};
