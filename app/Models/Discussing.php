<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class Discussing extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $table = 'forum';
    protected $fillable = ['created_user_id', 'discussing_title', 'discussing_text', 'discussing_category_id'];
    protected $primaryKey = 'reply_id';

    protected function createDiss(array $data)
    {

        $sql = DB::insert("INSERT INTO forum (`created_user_id`,
                   `discussing_title`,`discussing_text`,
                `discussing_category_id`,`click_count`,`date_added`,`image`) VALUES (" . $data['user_id'] . ",'" . $data['baslik'] . "','" . $data['note'] . "','" . $data['kategori'] . "',1,NOW(),'".(isset($data['image'])?$data['image']:null)."');");
        if ($sql == true) {
            return true;
        }
        return false;
    }

    public static function getList($data)
    {

        $where = [];

        if (!empty($data['text'])) {
            $where [] = "WHERE tbl1.discussing_text LIKE '%" . $data['text'] . "%' ";
        }
        if (!empty($data['category']) && empty($data['text'])) {
            $where[] = "WHERE tbl1.discussing_category_id = " . $data['category'] . " ";
        } else if (!empty($data['category'])) {
            $where[] = "AND tbl1.discussing_category_id = " . $data['category'] . " ";
        }

        $populer = ' ';
        if (!empty($data['populer'])) {
            $populer = "ORDER BY tbl1.click_count DESC";
        }

        if (!empty($data['resolved'])) {
            $where[] = "WHERE tbl1.status = 1 ";
        }

        if (!empty($data['not-yet-resolved'])) {
            $where[] = "WHERE tbl1.status = 0 ";
        }

        $noAnswer = ' ';
        if (!empty($data['no-answer-given'])) {
            $noAnswer = "HAVING reply_count = 0 ";
        }


        $sql = DB::select("SELECT tbl1.*,COUNT(r.reply_id) AS reply_count
                            FROM (SELECT f.* , u.name
                                    FROM forum f
                                    LEFT JOIN users u ON f.created_user_id = u.id
                                      ) AS tbl1
                                LEFT JOIN  reply r ON tbl1.discussing_id = r.discussing_id
                                " . implode(" ", $where) . "
                                 GROUP BY tbl1.discussing_id
                                   " . $populer . "
                                   " . $noAnswer . "
                                  LIMIT " . $data['pageFirsResult'] . ',' . $data['resultPerPage'] . ";");

        if (count($sql) > 0) {
            $sql = array_map(function ($a) {
                return (array)$a;
            }, $sql);
            return $sql;
        }
        return null;
    }

}
