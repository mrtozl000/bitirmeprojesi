<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function where(mixed $email)
    {
        $sql = DB::select("SELECT * FROM `users` WHERE `email`='" . $email . "'");
        if (count($sql)>0){
            return $sql;
        }
        return false;
    }

    public static function getUserInfo(mixed $id)
    {
        $sql = DB::select("SELECT `name`,`nickname`,`email`,`telephone`,`user_type_id` FROM `users` WHERE `id`='" . $id . "'");
        return $sql;
    }

    protected function create(array $data)
    {
        $sql = DB::insert("INSERT INTO `users` (name ,nickname,email,password,telephone,`user_type_id`,created_at) VALUES ('" . $data['name'] . "','" . $data['nickname'] . "','" . $data['email'] . "','".$data['password']."','".$data['telephone']."',5,NOW())");
        if ($sql) {
            return back()->with('succes', 'Kayıt yapıldı');
        } else {
            return back()->with('error', 'Kayıt yapılamadı');
        }
    }


}
