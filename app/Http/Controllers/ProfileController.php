<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Discussing;
use App\Models\Article;
use App\Models\Reply;

class ProfileController extends Controller
{
    public function profile()
    {
        if (Auth::user()) {
            $user_info = User::join('users_type', 'users_type.user_type_id', '=', 'users.user_type_id')->
            where('users.id', Auth::id())->get();

            $tab = Discussing::join('users', 'created_user_id', '=', 'users.id')->
            where('users.id', Auth::id())->simplePaginate(5);

            $tab2 = Reply::join('users', 'user_id', '=', 'users.id')->
            where('users.id', Auth::id())->simplePaginate(5);

            $tab3 = Article::join('users', 'created_user_id', '=', 'users.id')->
            where('users.id', Auth::id())->simplePaginate(5);

            return view('profile')->with(['profile',
                'user_info' => $user_info, 'tab' => $tab, 'tab2' => $tab2, 'tab3' => $tab3]);

        } else {
            return redirect()->back()->with(['error' => 'Üye Girişi Yapılmadan Profil sayfası Görüntülenemez']);
        }

    }

    public function update(Request $request,)
    {

        User::find($request['id'])->update(['name' => $request['name'], 'nickname' => $request['nickname'], 'telephone' => $request['telephone']]);
        return redirect()->back()->with(['status' => 'Profil bilgileriniz başarılı ile güncellendi']);
    }

}
