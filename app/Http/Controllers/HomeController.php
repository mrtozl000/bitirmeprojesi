<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Discussing;
use App\Models\Reply;
use App\Models\Article;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        $tab = Discussing::join('users', 'created_user_id', '=', 'users.id')->
        orderBy('date_added', 'DESC')->get(['forum.*', 'users.name'])->take(3);

        $tab2 = Reply::join('users', 'user_id', '=', 'users.id')->
        orderBy('reply_liked_count', 'DESC')->get(['reply.*', 'users.name'])->take(3);

        $tab3 = Article::join('users', 'created_user_id', '=', 'users.id')->
        orderBy('hit', 'DESC')->get(['articles.*', 'users.name'])->take(3);

        return view('home')->with(['tab' => $tab, 'tab2' => $tab2, 'tab3' => $tab3]);
    }
}
