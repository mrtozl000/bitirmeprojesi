<?php

namespace App\Http\Controllers;

use App\Mail\IletisimVeDestek;
use Illuminate\Http\Request;
use Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Mail;
use App\Models\Contact;

class ContactController extends Controller
{

    public function contact()
    {
        return view('contact');
    }

    public function contactpost(Request $request)
    {
        $rules=[
            'name'=>'required|min:5',
            'email'=>'required|email',
            'topic'=>'required',
            'message'=>'required|min:10'
        ];
        $validate = Validator::make($request->post(),$rules);

        if ($validate->fails()) {
            //print_r($validate->errors()->first('message'));
            return redirect()->route('contact')->withErrors($validate)->withInput();
        }

        $email = $request->email;

        $mailInfo = [
            'title' => $request->topic,
            'url' => 'http://dev.bitirme.localhost/',
            'body'=> $request->message,
        ];

        Mail::to($email)->send(new IletisimVeDestek($mailInfo));

        $contact = new Contact;
        $contact->name=$request->name;
        $contact->email=$request->email;
        $contact->topic=$request->topic;
        $contact->message=$request->message;
        $contact->save();
        return redirect()->route('contact')->with('success','İletişim mesağınız bize iletilmiştir. En kısa sürede mesajınızın yanıtı size iletilecektir.');
    }
}
