<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;

class LoginController extends Controller
{

    public function customLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        $user_info = User::where($request['email']);
        if ($user_info == false){
            return Redirect::back()->withErrors(['Fail' => 'Kullanıcı adı veya Parola yanlış']);
        }
        if (hash::check($request['password'], $user_info[0]->password)) {
            $credentials = $request->only('email', 'password');
            if (Auth::attempt($credentials)) {
                return redirect()->back()->with(['success' => 'Log You In']);
            }
        } else {
            return redirect()->back()->withErrors(['Fail' => 'Kullanıcı adı veya Parola yanlış']);
        }
    }

    public function customRegister(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'nickname' => 'required|max:30|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'telephone' => 'max:10'
        ]);

        $create = $this->create($request->all());

        $credentials = $request->only('email', 'password');

        if ($create == true){
            Auth::attempt($credentials);

            return Redirect::to('/home');
        }

        return redirect()->back()->with('error','Üye olunamadı');
    }

    private function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'nickname' => $data['nickname'],
            'telephone' => $data['telephone'],
            'password' => Hash::make($data['password'],
            )
        ]);
    }

    public function customLogout()
    {
        session::flush();
        Auth::logout();
        return redirect()->route('home.page');
    }
}
