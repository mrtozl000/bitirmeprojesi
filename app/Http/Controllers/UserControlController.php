<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Discussing;
use App\Models\Reply;
use App\Models\User;
use App\Models\UserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserControlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->user_type_id != 1){
            header('HTTP/1.1 404 Not Found');
            abort(404);
        }

        $userList= DB::table('users')->leftJoin('users_type', 'users.user_type_id', '=', 'users_type.user_type_id')->paginate(20);

        return view('UserControl/userList')->with(['list' => $userList]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user_info = User::join('users_type', 'users_type.user_type_id', '=', 'users.user_type_id')->
        where('users.id', $id)->get();

        $tab = Discussing::join('users', 'created_user_id', '=', 'users.id')->
        where('users.id', $id)->get();

        $tab2 = Reply::join('users', 'user_id', '=', 'users.id')->
        where('users.id', $id)->get();

        $tab3 = Article::join('users', 'created_user_id', '=', 'users.id')->
        where('users.id', $id)->get();

        $userTypes = UserType::all();

        return view('UserControl/userDetail')->with(['profile',
            'user_info' => $user_info, 'tab' => $tab, 'tab2' => $tab2, 'tab3' => $tab3,'usersType' => $userTypes]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateUserType = User::find($request['userId'])->update(['user_tpye_id'=>$id]);
         if ($updateUserType == true){
             toastr()->success('Kullanıcı Tipi Güncellenmiştir', 'Kullanıcı Tipi');
             return redirect()->route('admin-users-control.show',$request['userId']);
         }
        toastr()->error('Güncellenemedi.');
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Reply::where('user_id',$id)->delete();

        Discussing::where('created_user_id',$id)->delete();

        $deleteUserType = User::findOrFail($id)->delete();

        if ($deleteUserType == true){
               toastr()->success('Kullanıcı Güncelleme, Başarıyla Güncellenmiştir');
            return redirect()->route('admin-users-control.index');
        }
        toastr()->error('Silinemedi.');
        return redirect()->back();
    }

    public function search(Request $request)
    {
       $users = User::where('name','like',$request['search'])->get();

        return view('UserControl/userList')->with(['list' => $users]);
    }
}
