<?php

namespace App\Http\Controllers;

use App\Models\BlogCategory;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\File;
use App\Models\Article;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (in_array(Auth::user()->user_type_id, [1, 3])) {
            $articles = Article::where('created_user_id', Auth::user()->id)->orderBy('created_at', 'ASC')->get();
            return view('articles.index', compact('articles'));
        }
        toastr()->error('Giriş izniniz bulunmamaktadır!');
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_type_id = User::find(Auth::user()->id)->user_type_id;
        if (in_array($user_type_id, [1, 3])) {
            $categories = BlogCategory::all();
            return view('articles.create', compact('categories'));
        }
        toastr()->error('Giriş izniniz bulunmamaktadır!');
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'title' => 'min:3',
            'image' => 'required|image|mines:jpeg,png,jpg|max:2048',
            'content' => 'required',
        ]);
        if (in_array(Auth::user()->user_type_id, [1, 3])) {
            $article = new Article;
            $article->created_user_id = $request->created_user_id;
            $article->title = $request->title;
            $article->category_id = $request->category;
            $article->content = $request->article_content;
            $article->slug = Str::slug($request->title);

            if ($request->hasFile('image')) {
                $imageName = Str::slug($request->title) . '.' . $request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads'), $imageName);
                $article->image = '/uploads/' . $imageName;
            }
            $article->save();
            toastr()->success('Başarılı', 'Makale başarılı şekilde oluşturuldu.');
            return redirect()->route('yazilarim.index');
        }
        toastr()->error('Giriş izniniz bulunmamaktadır!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /** php artisan make:controller Back\ArticleController --resource
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (in_array(Auth::user()->user_type_id, [1, 3])) {
            $article = Article::findOrFail($id);
            $categories = BlogCategory::all();
            return view('articles.update', compact('categories', 'article'));
        }
        toastr()->error('Giriş izniniz bulunmamaktadır!');
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'title' => 'min:3',
            'image' => 'image|mines:jpeg,png,jpg|max:2048',
        ]);
        if (in_array(Auth::user()->user_type_id, [1, 3])) {
            $article = Article::findOrFail($id);
            $article->created_user_id = $request->created_user_id;
            $article->title = $request->title;
            $article->category_id = $request->category;
            $article->content = $request->article_content;
            $article->slug = Str::slug($request->title);

            if ($request->hasFile('image')) {
                $imageName = Str::slug($request->title) . '.' . $request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads'), $imageName);
                $article->image = '/uploads/' . $imageName;
            }
            $article->save();
            toastr()->success('Başarılı', 'Makale başarılı şekilde güncellendi.');
            return redirect()->route('yazilarim.index');
        }
        toastr()->error('Giriş izniniz bulunmamaktadır!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        if (in_array(Auth::user()->user_type_id, [1, 3])) {
            $article = Article::findOrFail($id);
            if (File::exists($article->image)) {
                File::delete(public_path($article->image));
            }
            Article::find($id)->delete();
            toastr()->success('Makale, silinen makalelere taşındı.');
            return redirect()->route('yazilarim.index');
        }
        toastr()->error('Giriş izniniz bulunmamaktadır!');
        return redirect()->back();
    }

}
