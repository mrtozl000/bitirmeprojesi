<?php

namespace App\Http\Controllers;


use App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\BlogCategory;
use App\Models\Article;
use App\Models\Config;

class BlogController extends Controller
{
    public function __construct()
    {
        /*
        if (Config::find(1)->active == 0) {
            return redirect()->to('aktif-degil')->send();
        }*/

        view()->share('blog_categories',BlogCategory::inRandomOrder()->get());
        //view()->share('config', Config::find(1));
    }

    public function index()
    {
        $data['articles'] = Article::with('getCategory')->orderBy('created_at','DESC')->simplePaginate(5);
        $data['articles']->withPath(url('blogs'));
        return view('homepage', $data);
    }

    public function single($category, $slug)
    {
        $category = BlogCategory::whereSlug($category)->first() ?? abort(403, 'Böyle bir kategori bulunmadı !!!');
        $article = Article::whereSlug($slug)->whereCategoryId($category->id)->first() ?? abort(403,'Böyle bir yazı bulunamadı !!!');
        $article->increment('hit');
        $data['article'] = $article;
        return view('single', $data);
    }

    public function category($category)
    {
        $categories = BlogCategory::whereSlug($category)->first() ?? abort(403, 'Böyle bir kategori bulunmadı !!!');
        $articles = Article::where('category_id', $categories->id)->where('status',1)->orderBy('created_at','DESC')->paginate(2);
        return view('category', ['category' =>$categories, 'articles'=>$articles]);
    }
}
