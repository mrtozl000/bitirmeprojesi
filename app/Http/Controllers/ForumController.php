<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Container\Container;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

use App\Models\Discussing;
use App\Models\Reply;
use App\Models\ForumCategory;
use Illuminate\Support\Str;

class ForumController extends Controller
{
    public function index(Request $request)
    {
        $category = ForumCategory::all();

        $data = [];


        $resultPerPage = 5;

        $data['page'] = 1;

        $connectForum = DB::table('forum')->get();

        $countOfDiscussing = count($connectForum);

        $numberOfPage = ceil($countOfDiscussing / $resultPerPage);

        if (!empty($request->get('page')) && $request->get('page') != 1) {
            $data['page'] = $request->get('page');
        }

        $pageFirstResult = ($data['page'] - 1) * $resultPerPage;

        $data['pageFirsResult'] = $pageFirstResult;

        $data['resultPerPage'] = $resultPerPage;

        if (!empty($request->get('q')) || !empty($request->get('category'))) {
            $data['text'] = $request->get('q');
            $data['category'] = $request->get('category');
        }

        if ($request->has('populer')){
            $data['populer'] = true;
        }
        if ($request->has('resolved')){
            $data['resolved'] = true;
        }
        if ($request->has('not-yet-resolved')){
            $data['not-yet-resolved'] = true;
        }
        if (!empty($request->has('no-answer-given'))){
            $data['no-answer-given'] = true;
        }

        $list = $this->getDiscussingList($data);

        if ($list === false) {
            return redirect()->back()->with(['status' => 'içerik bulunamadı.']);
        }

        return view('Forum/forum_list')->with(['list' => $list, 'category' => $category,'page' =>$numberOfPage, 'x' => pathinfo((new \Illuminate\Http\Request)->fullUrl())['basename']]);

    }


    public function getDiscussingList($data)
    {
        $dataQ = [];

        if (!empty($data['category'])) {
            $dataQ['category'] = ($data['category'] == 0) ? null : $data['category'];
        }
        if (!empty($data['text'])) {
            $dataQ['text'] = ($data['text'] == '') ? null : $data['text'];
        }

        if (!empty($data['populer'])){
            $dataQ['populer'] = true;
        }
        if (!empty($data['resolved'])){
            $dataQ['resolved'] = true;
        }
        if (!empty($data['not-yet-resolved'])){
            $dataQ['not-yet-resolved'] = true;
        }
        if (!empty($data['no-answer-given'])){
            $dataQ['no-answer-given'] = true;
        }

        $dataQ['pageFirsResult'] = $data['pageFirsResult'];

        $dataQ['resultPerPage'] =  $data['resultPerPage'];

        $array = array();

        $getDiscussing = Discussing::getList($dataQ);

        if ($getDiscussing != null) {
            foreach ($getDiscussing as $key => $value) {
                $array[$key] = $value;
            }
            return $array;
        } else {
            return false;
        }


    }

    public function newDiscussing(Request $request)
    {
        $request->validate([
            'threadTitle' => 'required',
            'summernote' => 'required',
            'category' => 'required',
        ]);

        $discussing_array = array();

        $discussing_array = [
            'user_id' => Auth::id(),
            'baslik' => $request['threadTitle'],
            'kategori' => $request['category'],
            'note' => $request['summernote'],
        ];

        //todo: dosya upload sistemi kurulacak.

         if (!empty($request->customFile)) {
             $imageName = Str::slug($request->title).'.'.$request->customFile->getClientOriginalExtension();
             $request->customFile->move(public_path('forumImages'), $imageName);
             $discussing_array = ['image'=>'/forumImages/'.$imageName];
         }

        $create = Discussing::createDiss($discussing_array);

        if ($create == true) {
            return redirect()->back()->with(['Success' => 'Konu oluşturulmuştur.']);
        } else {
            return redirect()->back()->with(['Error' => 'Konu oluşturulamadı.']);
        }

    }

    public function getDiscussingDetail($id, $page_number = null)
    {


        $getDiscussing = Discussing::join('users', 'created_user_id', '=', 'users.id')->
        get(['forum.*', 'users.name', 'users.user_type_id'])->where('discussing_id', '=', $id)->first();

        if (empty($getDiscussing->discussing_id)) {
            return redirect()->back()->with(['Error' => 'Herhangi bir Konu bulunamadı.']);
        }

        $clickCount = ($getDiscussing->click_count) + 1;

        Discussing::where('discussing_id', '=', $id)->update(['click_count' => $clickCount]);

        $countReply = DB::table('reply')->where('discussing_id', '=', $id)->count();


        $getReply = DB::table('reply')->leftJoin('users', 'user_id', '=', 'users.id')->
        where('discussing_id', '=', $id)->orderBy('reply_liked_count', 'DESC')->simplePaginate(5);


        return view('Forum/forum_detail')->with(['reply' => $getReply, 'discussing' => $getDiscussing, 'countFromReply' => $countReply]);

    }


    public function newReply(Request $request, $id)
    {

        $request->validate([
            'summernote' => 'required',
        ]);

        Reply::create([
            'discussing_id' => $id,
            'user_id' => Auth::id(),
            'reply_text' => $request['summernote'],
            'date_added' => now()
        ]);

        return redirect()->back()->with(['Success' => 'Başarıyla oluşturuldu']);

    }

    public function deleteDiscussing(Request $request,$delete_id)
    {
        Discussing::where('discussing_id', $delete_id)
            ->delete(['discussing_id' => $request['deleteId']]);

        return redirect('forum')->with(['Success' => 'Konu başarılı bir şekidle silinmiştir.']);
    }

    public function fixDiscussing(Request $request, $fixed_id)
    {
        Discussing::where('discussing_id', $fixed_id)
            ->update(['status' => $request['fixed']]);

        return redirect()->back()->with(['Success' => 'Konu çözüldü olarak güncellenmiştir.']);
    }

    public function updateDiscussing(Request $request, $id)
    {

        Discussing::where('discussing_id', $id)
            ->update(['discussing_title' => $request['updateDiscussingTitle'], 'discussing_text' => $request['updateDiscussingText']]);

        return redirect()->back()->with(['Success' => 'Başarıyla güncellendi']);

    }

    public function deleteReply(Request $request)
    {

        Reply::where('reply_id', $request['deleteReplyById'])->delete();

        return redirect()->back()->with(['status' => 'Başarıyla silindi']);
    }

    public function updateReply(Request $request,$id)
    {

        Reply::where('reply_id', $request['updateReplyById'])->update(['reply_text' => $request['updateNote']]);

        return redirect()->back()->with(['status' => 'Başarıyla güncellendi']);

    }

    public function likedReply($id, $reply_id)
    {
        $reply = Reply::where('reply_id', $reply_id);
        $reply->increment('reply_liked_count');
        return redirect()->back();
    }

}
